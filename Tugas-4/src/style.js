import {StyleSheet} from 'react-native';

export const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    flex: 1,
    marginBottom: '60%',
    width: '90%',
    alignItems: 'center',
    marginTop: 50,
    borderRadius: 10,
    backgroundColor: 'white',
  },
  shadowProp: {
    shadowColor: '#000',
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 7,
  },
  logo: {
    width: '100%',
    height: 300,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
  textStyling: {
    marginTop: 10,
    fontSize: 20,
    color: 'black',
  },
  textBinar: {
    fontSize: 16,
  },
  isiContent: {
    margin: 6,
    color: 'black',
    textAlign: 'justify',
  },
  footer: {
    flex: 1,
    flexDirection: 'row',
    alignContent: 'space-around',
    marginTop: 10,
  },
  textFooterSatu: {
    flex: 1,
    fontSize: 20,
    color: 'green',
    textAlign: 'left',
    paddingLeft: 20,
  },
  textFooterDua: {
    flex: 1,
    fontSize: 20,
    color: 'green',
    textAlign: 'right',
    paddingRight: 20,
  },
});
