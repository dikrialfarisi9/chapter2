/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {Text, View, Image} from 'react-native';

import {style} from './src/style';

class App extends React.Component {
  render() {
    return (
      <View style={style.container}>
        <View style={[style.content, style.shadowProp]}>
          <Image style={style.logo} source={require('./src/asset/logo.png')} />
          <Text style={style.textStyling}>Styling di React Native</Text>
          <Text style={style.textBinar}>Binar Academy - React Native</Text>
          <Text style={style.isiContent}>
            As a component grows in complexity, it is much cleaner and efficient
            to use StyleSheet.create so as to define several styles in one
            place.
          </Text>
          <View style={style.footer}>
            <Text style={style.textFooterSatu}>Understood!</Text>
            <Text style={style.textFooterDua}>What!</Text>
          </View>
        </View>
      </View>
    );
  }
}

export default App;
